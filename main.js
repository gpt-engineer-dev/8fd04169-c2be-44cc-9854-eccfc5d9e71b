// 1. Initialize game state variables
let currentPlayer = 'X';
let gameActive = true;
let gameState = ['', '', '', '', '', '', '', '', ''];

// 2. Set up event listeners
document.querySelectorAll('.grid > div').forEach(cell => cell.addEventListener('click', cellClicked));
document.getElementById('resetButton').addEventListener('click', resetGame);

// 3. Handle cell clicks
function cellClicked(event) {
  const cell = event.target;
  const cellIndex = parseInt(cell.getAttribute('data-cell-index'));

  // Check if the cell has already been played, or the game is paused
  if (gameState[cellIndex] !== '' || !gameActive) {
    return;
  }

  // Update game state
  gameState[cellIndex] = currentPlayer;
  cell.textContent = currentPlayer;

  // 4. Check for win or tie
  const winner = checkWinner();
  if (winner) {
    updateGameStatus(winner + ' wins!');
    gameActive = false;
    return;
  }

  const isTie = gameState.every(state => state !== '');
  if (isTie) {
    updateGameStatus('It\'s a tie!');
    gameActive = false;
    return;
  }

  // Switch player
  currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
  updateGameStatus(currentPlayer + '\'s turn');
}

// Check for a winner
function checkWinner() {
  // Define winning combinations
  const winningCombinations = [
    [0, 1, 2], [3, 4, 5], [6, 7, 8], // rows
    [0, 3, 6], [1, 4, 7], [2, 5, 8], // columns
    [0, 4, 8], [2, 4, 6]             // diagonals
  ];

  // Check for winning combinations
  for (const combination of winningCombinations) {
    const [a, b, c] = combination;
    if (gameState[a] && gameState[a] === gameState[b] && gameState[a] === gameState[c]) {
      return gameState[a];
    }
  }
  return null;
}

// Update game status message
function updateGameStatus(message) {
  document.getElementById('gameStatus').textContent = message;
}

// 5. Reset game
function resetGame() {
  currentPlayer = 'X';
  gameActive = true;
  gameState = ['', '', '', '', '', '', '', '', ''];
  document.querySelectorAll('.grid > div').forEach(cell => cell.textContent = '');
  updateGameStatus(currentPlayer + '\'s turn');
}
